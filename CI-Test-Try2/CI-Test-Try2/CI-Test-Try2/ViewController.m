//
//  ViewController.m
//  CI-Test-Try2
//
//  Created by Elliott, Brian on 12/29/15.
//  Copyright © 2015 Focus on the Family. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
