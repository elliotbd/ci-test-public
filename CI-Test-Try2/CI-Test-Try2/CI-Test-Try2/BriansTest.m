//
//  BriansTest.m
//  CI-Test-Try2
//
//  Created by Elliott, Brian on 12/29/15.
//  Copyright © 2015 Focus on the Family. All rights reserved.
//

#import "BriansTest.h"

@implementation BriansTest

-(NSString *)concatTwoStrings:(NSString *)firstString secondString:(NSString *)secondString {
    
    return [NSString stringWithFormat:@"%@-%@", firstString, secondString];
    
}

@end
