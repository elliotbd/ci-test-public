//
//  AppDelegate.h
//  CI-Test-Try2
//
//  Created by Elliott, Brian on 12/29/15.
//  Copyright © 2015 Focus on the Family. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

