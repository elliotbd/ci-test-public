//
//  BriansTest.h
//  CI-Test-Try2
//
//  Created by Elliott, Brian on 12/29/15.
//  Copyright © 2015 Focus on the Family. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BriansTest : NSObject

-(NSString *)concatTwoStrings:(NSString *)firstString secondString:(NSString *)secondString;

@end
