//
//  FirstTest.m
//  CI-Test-Try2
//
//  Created by Elliott, Brian on 12/29/15.
//  Copyright © 2015 Focus on the Family. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BriansTest.h"

@interface FirstTest : XCTestCase

@property (nonatomic) BriansTest *briansTest;

@end

@implementation FirstTest

- (void)setUp {
    [super setUp];
    
    self.briansTest = [[BriansTest alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    
    NSString *firstString = @"Brian";
    NSString *secondString = @"Elliott";
    NSString *resultingString = [self.briansTest concatTwoStrings:firstString secondString:secondString];
    NSLog(@"testExample concatTwoStrings results: %@", resultingString);
    
    NSString *expectedString = @"Brian-Elliott";
    XCTAssertEqualObjects(expectedString, resultingString);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
